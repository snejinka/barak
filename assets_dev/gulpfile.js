/*global require*/

var WORK_OUT_FOLDER = '../public_html/assets/',
    PROD = false;

var gulp        = require('gulp'),
    plumber     = require('gulp-plumber'),
    size        = require('gulp-filesize'),

    spritesmith = require('gulp.spritesmith'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
	buffer      = require('vinyl-buffer'),
    merge       = require('merge-stream'),

    autoprefixer = require('gulp-autoprefixer'),
    minifyCSS   = require('gulp-minify-css'),

    babel       = require('gulp-babel'),
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    sass        = require('gulp-sass'),
    uglify      = require('gulp-uglify'),
    browserSync = require('browser-sync').create();


gulp.task('js', function () {
    "use strict";

    return gulp.src(['js/**/*.js'])
        .pipe(plumber())
        .pipe(babel())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js/'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'))
        .pipe(size());
});


gulp.task('vendors', function () {
    "use strict";

    gulp.src([
    ])
    .pipe(concat('vendors.css'))
    // .pipe(rename('vendors.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));

    var vendors_js = [
    ];

    gulp.src(vendors_js)
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'))
        .pipe(rename('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'));

});

//
// gulp.task('sprite', function () {
//     "use strict";
//
//     var spriteData = gulp.src('img/*.png')
//         .pipe(plumber())
//         .pipe(spritesmith({
//             padding: 20,
//             imgName: '../img/sprite.png',
//             cssName: 'sprite.scss'
//         }));
//
//     var imgStream = spriteData.img
//         .pipe(buffer())
//         .pipe(imagemin({
//             progressive: true,
//             svgoPlugins: [{removeViewBox: false}],
//             use: [pngquant()]
//         }))
//         .pipe(gulp.dest(WORK_OUT_FOLDER + 'img/'));
//
//
//     var cssStream = spriteData.css
//         .pipe(gulp.dest('scss/'));
//
//     return merge(imgStream, cssStream);
//
// });

gulp.task('serve', ['scss-to-css'], function() {

    browserSync.init({
        server: "../public_html"
    });

    gulp.watch("scss/*.scss", ['scss-to-css']);
    gulp.watch("../public_html/*.html").on('change', browserSync.reload);
});

gulp.task("scss-to-css", function () {
    "use strict";

    var stream = gulp.src("scss/app.scss")
        .pipe(sass())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'))
        .pipe(rename({suffix: ".min"}))
        .pipe(minifyCSS())
        .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));
});

gulp.task('watch', function () {
    "use strict";

    gulp.watch('scss/**/*.scss', ['scss-to-css']);
    gulp.watch('js/**/*.js', ['js']);


});

gulp.task('default', ['watch']);